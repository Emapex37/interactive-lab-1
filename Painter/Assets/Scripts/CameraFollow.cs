﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public GameObject player;
    Camera mainCam;

    // Start is called before the first frame update
    void Start()
    {

        player = GameObject.Find("Player");
        mainCam = GetComponent<Camera>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {

        mainCam.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);

    }
}
