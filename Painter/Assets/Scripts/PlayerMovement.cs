﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    //Keyboard input
    float xIn, yIn;
    //Player speed
    public float pSpeed = 50f;
    //Max speed
    public float pMaxSpeed = 5;
    //Jump height
    public float pJumpMax = 300f;

    public bool grounded;

    private Rigidbody2D rigB;

    // Start is called before the first frame update
    void Start()
    {
        rigB = gameObject.GetComponent<Rigidbody2D>();
        rigB.freezeRotation = true;
        rigB.gravityScale = 1.5f;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.W) && grounded == true)
        {
            rigB.AddForce(Vector2.up * pJumpMax);
            //print("jump");
        }

    }

    void FixedUpdate()
    {
        //Moving player
        float f = Input.GetAxis("Horizontal");
        rigB.AddForce((Vector2.right * pSpeed) * f);

        //Capping speed so they don't reach supersonic levels
        if (rigB.velocity.x > pMaxSpeed)
        {
        rigB.velocity = new Vector2(pMaxSpeed, rigB.velocity.y);
        }
        if (rigB.velocity.x < -pMaxSpeed)
        {
            rigB.velocity = new Vector2(-pMaxSpeed, rigB.velocity.y);
        }
    }
}
