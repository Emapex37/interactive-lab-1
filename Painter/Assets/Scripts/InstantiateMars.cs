﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateMars : MonoBehaviour
{

    public CircleCollider2D mars;

    // Start is called before the first frame update
    void Start()
    {

        CircleCollider2D marsClone = (CircleCollider2D)Instantiate(mars, transform.position, transform.rotation);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
