﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateUpDown : MonoBehaviour
{

    int curDir;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y <= -5)
        {
            curDir = 1;
        } else if (transform.position.y >= 0)
        {
            curDir = 0;
        }

        if (curDir == 1)
        {
            transform.Translate(Vector2.up * Time.deltaTime);
        } else
        {
            transform.Translate(Vector2.down * Time.deltaTime);
        }
        
    }
}
