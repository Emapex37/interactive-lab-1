﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarsTrigger : MonoBehaviour
{

    public GameObject player;
    public Rigidbody2D playerRigB;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        playerRigB = player.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        playerRigB.gravityScale = 0;
        playerRigB.velocity = new Vector2(0, 0);
    }

    
}
